/**
 * Обработчики для нажатий ссылок.
 */
$(document).on('click', 'a.nav-link, a.dropdown-item', function () {
    let leftMenu = $('#left_menu');

    // Подгружает данные в левое меню если у ссылки есть аттрибут "data-link-menu".
    if ($(this).is('[data-link-menu]')) {
        leftMenu.html(pages[$(this).data('link-menu')]);
    }

    // Аналогично с контентом и аттрибутом "data-link-content".
    if ($(this).is('[data-link-content]')) {
        $('#content').html(pages[$(this).data('link-content')]);
    }

    // Скрываем боковое меню на главной странице и тестировании / показываем на остальных.
    leftMenu.toggle(! $(this).is('#link_index, #link_quiz'));

    if ($(this).is('#link_quiz')) {
        startQuiz();
    }
});

/**
 * Смена активного элемента верхнего меню.
 */
$('.navbar-nav a.nav-link').click(function () {
    $(this).closest('ul.navbar-nav').find("li.active").removeClass("active");
    $(this).closest('li').addClass("active");
});

$(function () {
    $('#link_index').click(); // Подгружаем контент главной страницы при открытии index.html.
});

$(document).on('click', 'img', function () {
    $('.imagepreview').attr('src', $(this).attr('src'));
    $('#imagemodal').modal('show');
});

$(window).scroll(function() {
    if ($(this).scrollTop()) {
        $('#toTop').fadeIn();
    } else {
        $('#toTop').fadeOut();
    }
});

$("#toTop").click(function() {
    $("html, body").animate({scrollTop: 0}, 1000);
});
